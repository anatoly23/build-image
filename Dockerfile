FROM python:3.8

RUN apt-get update && \
	apt-get install -y --no-install-recommends docker.io curl

# Install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s  https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN curl -LO "https://dl.k8s.io/$(curl -L -s  https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install helm
RUN curl -LO https://get.helm.sh/helm-v3.7.0-linux-amd64.tar.gz && \
	tar -zxvf helm-v3.7.0-linux-amd64.tar.gz && \
	mv linux-amd64/helm /usr/local/bin/helm && \
	rm -rf helm-v3.7.0-linux-amd64.tar.gz linux-amd64